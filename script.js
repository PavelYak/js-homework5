function createNewUser() {

    let firstName = prompt('Enter your name', 'Pavel');
    let lastName = prompt('Enter your second name', 'Yakovenko');
    let birthday = prompt('Enter your birthday', '11.07.1997');


    return {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,

        getLogin: function() {
            return this.firstName.charAt(0).toLowerCase() + lastName.toLowerCase()
        },
        getAge: function() {
            let age = Date.now() - Date.parse(this.birthday);
            return (age / (1000 * 60 * 60 * 24 * 365)).toFixed();
        },
        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.substr(6,9);
        }
    };
}

let user01 = createNewUser();
console.log(user01.getLogin());
console.log(user01.getAge());
console.log(user01.getPassword());
